package cl.gechs.cursojava.webservices;

import cl.gechs.cursojava.webservices.data.UserServiceRequestType;
import cl.gechs.cursojava.webservices.data.UserServiceResponseType;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail.com>
 */
@WebService(targetNamespace = "http://www.gechs.cl/webservices/ws-user")
public interface UserWSI {

    @WebMethod
    public UserServiceResponseType userByIdRequest(UserServiceRequestType request);

    @WebMethod
    public UserServiceResponseType userListRequest();
}
