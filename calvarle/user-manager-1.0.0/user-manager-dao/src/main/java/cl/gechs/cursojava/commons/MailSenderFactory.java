package cl.gechs.cursojava.commons;

import java.util.List;
import java.util.Properties;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public class MailSenderFactory {

    private static final Logger LOGGER = Logger.getLogger(MailSenderFactory.class);
    private static Session gmailSession;

    public static MailSender makeMailSender(String[] emails, String subjectPrefix, String subject, String body, List<MailAttachment> attachments,
            MailSender.Type senderType, MailSenderConfig mailServerConfig) {
        switch (senderType) {
            case GMAIL:
                Session gSession = getGmailSession(mailServerConfig);
                if (gSession != null) {
                    return new GmailMailSender(emails, subjectPrefix, subject, body, attachments, mailServerConfig, gSession);
                } else {
                    LOGGER.error("Error creating Gmail Session!");
                    return null;
                }

            case EXCHANGE:
                //@todo to be implement
            default:
                LOGGER.error("Error creating mail sender, type not recognized: " + senderType);
                return null;
        }
    }

    private static Session getGmailSession(final MailSenderConfig mailServerConfig) {
        try {
            if (gmailSession == null) {
                Properties props = new Properties();
                props.put("mail.smtp.auth", mailServerConfig.getAuth());
                props.put("mail.smtp.starttls.enable", mailServerConfig.getStarttlsEnabled());
                props.put("mail.smtp.host", mailServerConfig.getHost());
                props.put("mail.smtp.port", mailServerConfig.getPort());
                gmailSession = Session.getInstance(props, new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(mailServerConfig.getUsername(), mailServerConfig.getPassword());
                    }
                });
            }
            return gmailSession;
        } catch (Exception ex) {
            LOGGER.error("Error creating Gmail Mail Sender: " + ex.getMessage(), ex);
            return null;
        }
    }

}
