package cl.gechs.cursojava.beans.users;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.beans.LoginBean;
import cl.gechs.cursojava.beans.UtilsBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Carlos Soza <carlos.soza@gmail.com>
 */
@ManagedBean(name = "userController")
@ViewScoped
public class UserController implements Serializable {

    private static final long serialVersionUID = -2247748432906724195L;
    // BEANS
    @ManagedProperty("#{utilsBean}")
    private UtilsBean utilsBean;
    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;
    // DATA
    private LazyDataModel<User> users;

    @PostConstruct
    public void init() {
        this.users = new UserDataModel(this);
    }

    public LazyDataModel<User> getUsers() {
        return this.users;
    }

    public UtilsBean getUtilsBean() {
        return utilsBean;
    }

    public void setUtilsBean(UtilsBean utilsBean) {
        this.utilsBean = utilsBean;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
}
