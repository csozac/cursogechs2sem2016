/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

/**
 *
 * @author calvarez
 */

import java.io.FileOutputStream;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
/**
 * 
 * @author calvarez
 */




public class GeneraExcel {
private static final Logger LOGGER = Logger.getLogger(GeneraExcel.class);
public void generaExcel(String filename) {
try {
Workbook workbook = new HSSFWorkbook();
Sheet sheet = workbook.createSheet("Alumnos");
Row rowhead = sheet.createRow((short) 0);
rowhead.createCell(0).setCellValue("Nombre");
rowhead.createCell(1).setCellValue("Rut");
rowhead.createCell(2).setCellValue("Correo");
Row row = sheet.createRow((short) 1);
row.createCell(0).setCellValue("Carlos Soza");
row.createCell(1).setCellValue("1286634-7");
row.createCell(2).setCellValue("carlos.soza@gmail.com");
FileOutputStream fileOut = new FileOutputStream(filename);
workbook.write(fileOut);
fileOut.close();
} catch (Exception ex) {
LOGGER.error("Error al generar el archivo: " + ex.getMessage(), ex);
}
}
}
