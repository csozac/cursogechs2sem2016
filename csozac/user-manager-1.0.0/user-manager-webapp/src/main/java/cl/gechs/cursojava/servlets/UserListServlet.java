package cl.gechs.cursojava.servlets;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail dot com>
 */
@WebServlet("/servlets/users/UserListServlet")
public class UserListServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(UserListServlet.class);
    private static final long serialVersionUID = 1L;

    private static final UsersService USERS_SERVICE;

    static {
        USERS_SERVICE = ServicesDirectory.getUserService();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        JsonObject output = new JsonObject();
        PrintWriter out = response.getWriter();

        try {
            List<User> users = USERS_SERVICE.getUsers();
            JsonArray userArray = new JsonArray();
            for (User u : users) {
                JsonObject uj = new JsonObject();
                uj.addProperty("nombre", u.getName());
                uj.addProperty("email", u.getEmail());
                uj.addProperty("roles", u.getRoles().toString());
                uj.addProperty("username", u.getUsername());
                userArray.add(uj);
            }
            output.add("user_list", userArray);
        } catch (Exception ex) {
            LOGGER.error("Cannot get User List: ", ex);
            output.addProperty("result", "ERROR");
        } finally {
            out.print(output.toString());
            out.close();
        }
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
