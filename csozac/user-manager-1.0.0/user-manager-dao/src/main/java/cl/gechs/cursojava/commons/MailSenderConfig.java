package cl.gechs.cursojava.commons;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public class MailSenderConfig {

    private String username;
    private String password;
    private String host;
    private String port;
    private String auth;
    private String starttlsEnabled;
    private String fromEmailAddress;
    private String fromEmailName;
    private boolean enabled;

    private MailSenderConfig() {
    }

    private MailSenderConfig(String username, String password, String host, String port, String auth, String starttlsEnabled, String fromEmailName,
            String fromEmailAddress, boolean enabled) {
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.auth = auth;
        this.starttlsEnabled = starttlsEnabled;
        this.fromEmailAddress = fromEmailAddress;
        this.fromEmailName = fromEmailName;
        this.enabled = enabled;
    }

    private MailSenderConfig(String username, String password, String host, String fromEmailName, String fromEmailAddress, boolean enabled) {
        this.username = username;
        this.password = password;
        this.host = host;
        this.fromEmailAddress = fromEmailAddress;
        this.fromEmailName = fromEmailName;
        this.enabled = enabled;
    }

    public static MailSenderConfig make(String username, String password, String host, String port, String auth, String starttlsEnabled, String fromEmailName,
            String fromEmailAddress, boolean enabled) {
        return new MailSenderConfig(username, password, host, port, auth, starttlsEnabled, fromEmailName, fromEmailAddress, enabled);
    }

    public static MailSenderConfig make(String username, String password, String host, String fromEmailName, String fromEmailAddress, boolean enabled) {
        return new MailSenderConfig(username, password, host, fromEmailName, fromEmailAddress, enabled);
    }

    public String getFromEmailAddress() {
        return fromEmailAddress;
    }

    public String getFromEmailName() {
        return fromEmailName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getAuth() {
        return auth;
    }

    public String getStarttlsEnabled() {
        return starttlsEnabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

}
