package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.enums.EmailNotificationType;
import cl.gechs.cursojava.model.EmailNotification;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class EmailNotificationDAO extends BaseDAO<EmailNotification> {

    private static final Logger LOGGER = Logger.getLogger(EmailNotificationDAO.class);

    @Override
    public Class<EmailNotification> getObjectClass() {
        return EmailNotification.class;
    }

    public EmailNotification getEmailNotification(String emailNotificationId) {
        Query query = this.getEntityManager().createQuery("FROM EmailNotification where emailNotificationId=:emailNotificationId");
        query.setParameter("emailNotificationId", emailNotificationId);
        try {
            return (EmailNotification) query.getSingleResult();
        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<EmailNotification> getEmailNotifications(List<EmailNotificationType> types) {
        try {
            Query query = this.getEntityManager().createQuery("FROM EmailNotification e where e.type in :types ");
            query.setParameter("types", types);
            return query.getResultList();

        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<EmailNotification> getEmailNotifications() {
        try {
            Query query = this.getEntityManager().createQuery("FROM EmailNotification");
            return query.getResultList();

        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }
}
