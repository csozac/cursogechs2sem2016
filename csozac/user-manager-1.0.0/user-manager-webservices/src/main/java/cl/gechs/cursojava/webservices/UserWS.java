package cl.gechs.cursojava.webservices;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import cl.gechs.cursojava.webservices.data.UserServiceRequestType;
import cl.gechs.cursojava.webservices.data.UserServiceResponseType;
import cl.gechs.cursojava.webservices.data.UserServiceResultType;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail.com>
 */
@WebService(
        portName = "UserWSPort",
        serviceName = "UserWS",
        targetNamespace = "http://www.gechs.cl/webservices/ws-user",
        endpointInterface = "cl.gechs.cursojava.webservices.UserWSI")
/**
 * URL WDSL: http://localhost:8080/user-manager-webservices/UserWS?wsdl
 */
public class UserWS implements UserWSI {

    private static final UsersService USERS_SERVICE;

    static {
        USERS_SERVICE = ServicesDirectory.getUserService();
    }

    @Override
    public UserServiceResponseType userByIdRequest(UserServiceRequestType request) {

        User user = USERS_SERVICE.getUser(Long.parseLong(request.getUserId()));
        UserServiceResponseType response = new UserServiceResponseType();
        response.setResult(UserServiceResultType.OK);
        response.setDescription(user.toString());
        return response;
    }

    @Override
    public UserServiceResponseType userListRequest() {
        List<User> users = USERS_SERVICE.getUsers();
        String resp = "";
        for (User u : users) {
            resp += u.toString() + "\n";
        }
        UserServiceResponseType response = new UserServiceResponseType();
        response.setResult(UserServiceResultType.OK);
        response.setDescription(resp);
        return response;
    }
}
