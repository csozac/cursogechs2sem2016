package cl.gechs.cursojava.beans.converters;

import cl.gechs.cursojava.model.UserRoleEnum;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@ManagedBean
@RequestScoped
@FacesConverter("userRoleConverter")
public class UserRoleConverter extends EnumConverter {

    public UserRoleConverter() {
        super(UserRoleEnum.class);
    }

}
