package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class ExportToExcel {

    private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);

    public static void main(String[] args) {
        LOGGER.info("Generando Archivo");
        GeneraExcel generaExcel = new GeneraExcel();
        // Usar Path de archivo de sus máquinas
        generaExcel.generaExcel("/Users/csozac/Downloads/Test.xls");
        LOGGER.info("Archivo Generado con Éxito");
    }
}
