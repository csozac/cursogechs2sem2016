package cl.gechs.export;
import java.io.File;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class ExportToExcelTest {

    private static final Logger LOGGER = Logger.getLogger(ExportToExcelTest.class);

    public ExportToExcelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        LOGGER.debug("Setup Class");
    }

    @AfterClass
    public static void tearDownClass() {
        LOGGER.debug("Tear Down Class");
    }

    @Before
    public void setUp() {
        LOGGER.debug("Setup");
    }

    @After
    public void tearDown() {
        LOGGER.debug("tearDown");
    }

    @Test
    public void testGenerarExcel() {
        String filename = "target/Test.xls";
        GeneraExcel generaExcel = new GeneraExcel();
        generaExcel.generaExcel(filename);
        File f = new File(filename);
        Assert.assertTrue("Se espera que el archivo Exista: " + filename, f.exists());
    }
    
    @Test
    public void testGenerarExcelException() {
        String filename = "temp/Test.xls";
        GeneraExcel generaExcel = new GeneraExcel();
        generaExcel.generaExcel(filename);
        File f = new File(filename);
        Assert.assertFalse("Se espera que el archivo Exista: " + filename, f.exists());
    }

    @Test
    public void testDumy() {
        Assert.assertTrue(true);
    }

    /**
     * Test of main method, of class ExportToExcel.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        ExportToExcel.main(args);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
}