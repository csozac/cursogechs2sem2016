package cl.gechs.cursojava.commons;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail.com>
 */
public class RandomUtils {

    private static final int ID_NUM_BITS = 130;
    private static final int ID_RADIX = 32;
    private static final SecureRandom RANDOM = new SecureRandom();

    private RandomUtils() {
    }

    public static String generateRandomID() {
        return new BigInteger(ID_NUM_BITS, RANDOM).toString(ID_RADIX);
    }

    public static String generateRandomID(int size) {
        return generateRandomID().substring(0, size);
    }

    public static boolean generateRandomBoolean(float p) {
        return RANDOM.nextFloat() < p;
    }

}
