package cl.gechs.cursojava.commons;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza  <carlos.soza at gmail.com>
 */
public class Attachment implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(Attachment.class);
    private static final long serialVersionUID = 3373380088108518568L;
    private String name;
    private String type;
    private String encoding;
    private transient byte[] data;

    public Attachment() {
        this.encoding = "UTF-8";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public static Attachment make(String location) {
        Attachment attachment = new Attachment();
        String path = new File(location).getAbsolutePath();
        String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
        try (InputStream stream = new BufferedInputStream(new FileInputStream(path))) {
            attachment.setName(FilenameUtils.getName(location));
            attachment.setType(contentType);
            attachment.setData(IOUtils.toByteArray(stream));
            stream.close();
        } catch (IOException ex) {
            LOGGER.error("Cannot make Attachment POJO", ex);
        }

        return attachment;
    }

}
