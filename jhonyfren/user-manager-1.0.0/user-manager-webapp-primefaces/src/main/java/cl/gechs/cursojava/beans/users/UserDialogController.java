package cl.gechs.cursojava.beans.users;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRoleEnum;
import cl.gechs.cursojava.beans.LoginBean;
import cl.gechs.cursojava.beans.UtilsBean;
import cl.gechs.cursojava.commons.BaseController;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@ManagedBean(name = "userDialogController")
@ViewScoped
public class UserDialogController extends BaseController implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(UserDialogController.class);
    private static final long serialVersionUID = -3947493904292228639L;
    // BEANS
    @ManagedProperty("#{utilsBean}")
    private UtilsBean utilsBean;
    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;
    // DATA
    private User user;

    @PostConstruct
    public void init() {
        this.resetUser();
    }

    public void resetUser() {
        this.user = new User();
    }

    public void doCreateUser() {
        // set user data
        LOGGER.debug("Saving New User " + this.user);
        this.user.setPassword(this.utilsBean.getUsersService().encryptUserPassword(this.user.getPassword()));
        if (this.utilsBean.getUsersService().saveUser(this.user)) {
            this.generateMainInfoMessage("Información", "Usuario guardaro correctamente");
        } else {
            this.generateMainErrorMessage("Error", "No se pudo guardar el Usuario");
        }
    }

    public void doUpdateUser() {
        // set user data
        LOGGER.info("Updating User: " + this.user);
        if (this.utilsBean.getUsersService().updateUser(this.user)) {
            this.generateMainInfoMessage("Información", "Usuario actualizado correctamente");
        } else {
            this.generateMainErrorMessage("Error", "No se pudo actualizar el Usuario");
        }
    }

    public void doDeleteUser(String username) {
        // set user data
        this.user = this.utilsBean.getUsersService().getUserByUsername(username);

        LOGGER.debug("Deleting User: " + this.user.getUsername());
        if (this.utilsBean.getUsersService().deleteUser(this.user)) {
            this.generateMainInfoMessage("Información", "Usuario eliminado correctamente");
        } else {
            this.generateMainErrorMessage("Error", "No se pudo eliminar el Usuario");
        }
    }

    public void doGeneratePassword(String username) {
        LOGGER.info("Generating password for User: " + username);
        this.user = this.utilsBean.getUsersService().getUserByUsername(username);
        if (this.user != null) {
            String password = this.utilsBean.getUsersService().generatePassword(this.user.getId());

            if (this.utilsBean.getUsersService().updateUser(this.user)) {
                this.generateMainInfoMessage("Información", "Password generada:" + " " + password);
            } else {
                this.generateMainErrorMessage("Error", "No se pudo generar password para user: " + username);
            }
        } else {
            LOGGER.warn("Selected User Id is NUll");
        }
    }

    @SuppressWarnings("all")
    public void validateUser(FacesContext context, UIComponent component, Object userName) {
        User euser = null;
        try {
            if (userName != null && !userName.toString().isEmpty()) {
                euser = this.utilsBean.getUsersService().getUserByUsername(userName.toString());
            }
        } catch (Exception ex) {
            LOGGER.warn("Error getting existing user: " + ex.getMessage());
        }
        if (euser != null) {
            FacesMessage msg = new FacesMessage("Username ya existe");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UtilsBean getUtilsBean() {
        return utilsBean;
    }

    public void setUtilsBean(UtilsBean utilsBean) {
        this.utilsBean = utilsBean;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public List<UserRoleEnum> getUserRoles() {
        return Arrays.asList(UserRoleEnum.values());
    }

}
