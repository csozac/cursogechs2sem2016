package cl.gechs.cursojava.beans;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRoleEnum;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(LoginBean.class);
    private static final long serialVersionUID = 7765876811740798583L;
    private static final String CANNOT_START_SESSION = "Cannot start Session";
    private static final String CANNOT_STOP_SESSION = "Cannot stop Session";
    // SESSION DATA
    private String username;
    private String password;
    private String errorMessage = "";
    private User userLogged;
    private HttpSession httpsession = null;
    private boolean loggedIn = false;
    // BEANS
    @ManagedProperty("#{navigationBean}")
    private NavigationBean navigationBean;
    @ManagedProperty("#{utilsBean}")
    private UtilsBean utilsBean;

    public String doLogin() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        try {
            if (username != null && password != null) {
                // validate user
                User user = this.utilsBean.getUsersService().getUserByUsername(username);
                if (user == null) {
                    userLogged = null;
                    return navigationBean.getLoginPath();
                } else {
                    userLogged = user;
                }
                request.login(username, password);

                this.httpsession = request.getSession();
                this.httpsession.setAttribute("user", username);
                this.httpsession.setAttribute("roles", user.getRoles());
                this.loggedIn = true;
                return navigationBean.getHomePath();
            }
        } catch (ServletException ex) {
            LOGGER.error(CANNOT_START_SESSION + " :", ex);
        }

        return navigationBean.getLoginPath();
    }

    public void doLogout(boolean redirect) {
        // Set the paremeter indicating that user is logged in to false
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            request.logout();
            request.getSession().invalidate();
            this.loggedIn = false;
            this.httpsession = null;
            this.userLogged = null;

            if (redirect) {
                this.redirectToLogin();
            }
        } catch (ServletException | IOException ex) {
            LOGGER.error(CANNOT_STOP_SESSION + " :", ex);
        }
    }

    public String getLoginPath() {
        return "/login.jsf";
    }

    public String getHomePath() {
        return "/pages/users/management.jsf";
    }

    public void redirectToLogin() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext()
                .redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + this.getLoginPath());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public User getUser() {
        return userLogged;
    }

    public UtilsBean getUtilsBean() {
        return utilsBean;
    }

    public void setUtilsBean(UtilsBean utilsBean) {
        this.utilsBean = utilsBean;
    }

    public User getUserLogged() {
        return userLogged;
    }

    public void setUserLogged(User userLogged) {
        this.userLogged = userLogged;
    }

    public String getUserRoles() {
        List<String> strRoles = new ArrayList<>();
        this.getUser().getRoles().stream().forEach(role -> strRoles.add(role.getId().toUpperCase()));
        return StringUtils.join(strRoles, ", ");
    }

    public boolean isUserInRoles(String values) {
        if (this.loggedIn) {
            List<UserRoleEnum> roles = new ArrayList<>();
            for (String value : values.split(",")) {
                roles.add(UserRoleEnum.getById(value.trim().toUpperCase()));
            }
            boolean resp = this.utilsBean.getUsersService().isUserInRoles(this.userLogged.getUsername(), roles);
            return resp;
        }
        return false;
    }
}
