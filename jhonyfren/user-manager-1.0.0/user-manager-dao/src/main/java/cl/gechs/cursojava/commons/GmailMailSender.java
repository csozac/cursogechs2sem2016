package cl.gechs.cursojava.commons;

import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;


/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public class GmailMailSender extends MailSender {

    private static final Logger LOGGER = Logger.getLogger(GmailMailSender.class);
    private final Session mailSession;
    private final MailSenderConfig config;

    public GmailMailSender(String[] emails, String subjectPrefix, String subject, String body, List<MailAttachment> attachments,
            MailSenderConfig config, Session mailSession) {
        super(emails, subjectPrefix, subject, body, attachments);
        this.mailSession = mailSession;
        this.config = config;
    }

    @Override
    public Boolean call() {
        if (this.mailSession == null) {
            LOGGER.error(CANNOT_SEND_EMAIL + "MailSession is NULL");
            return false;
        }
        
        if (!this.config.isEnabled()) {
            LOGGER.error(CANNOT_SEND_EMAIL + "MailConfig is DISABLED");
            return false;
        }

        try {
            MimeMessage message = new MimeMessage(this.mailSession);
            message.setHeader(CONTENT_TYPE, TEXT_HTML_CHARSET_UTF_8);
            message.setFrom(new InternetAddress(config.getFromEmailAddress(), config.getFromEmailName()));
            for (String email : emails) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            }
            message.setSubject("[" + subjectPrefix + "] " + this.subject, ENCODING);

            if (this.attachments != null) {
                // create the message part
                MimeBodyPart messageBodyPart = new MimeBodyPart();

                // fill message
                messageBodyPart.setContent(this.body, TEXT_HTML_CHARSET_UTF_8);

                Multipart multipart = new MimeMultipart(MIXED);
                multipart.addBodyPart(messageBodyPart);

                // Part two is attachments
                for (MailAttachment attachment : this.attachments) {
                    LOGGER.debug("Attachment: " + attachment);
                    Path path = Paths.get(attachment.getPath());

                    messageBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(attachment.getPath());
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(path.getFileName().toString());
                    if (attachment.isInline()) {
                        messageBodyPart.setContentID("cid-attachment-" + path.getFileName().toString());
                        messageBodyPart.setDisposition(MimeBodyPart.INLINE);
                    }
                    multipart.addBodyPart(messageBodyPart);
                }

                // Put parts in message
                message.setContent(multipart);
            } else {
                // only html message
                message.setContent(this.body, TEXT_HTML_CHARSET_UTF_8);
            }
            message.saveChanges();

            Transport transport = this.mailSession.getTransport(SMTP);
            transport.connect();
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            return true;
        } catch (MessagingException | UnsupportedEncodingException ex) {
            LOGGER.error(CANNOT_SEND_EMAIL, ex);
        }

        return false;
    }

}
