package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRoleEnum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail.com>
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class UserDAO extends BaseDAO<User> {

    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);

    @Override
    public Class<User> getObjectClass() {
        return User.class;
    }

    public List<UserRoleEnum> getUserRoles(String username) {
        Query query = this.getEntityManager().createQuery("select roles FROM User where username=:username");
        query.setParameter("username", username);
        try {
            return (List<UserRoleEnum>) query.getResultList();
        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<User> getUsers() {
        Query query = this.getEntityManager().createQuery("FROM User");
        try {
            return (List<User>) query.getResultList();
        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<User> getUsers(List<UserRoleEnum> roles) {
        try {
            List<User> users = new ArrayList<>();
            for (UserRoleEnum rol : roles) {
                Query query = this.getEntityManager().createQuery("FROM User u inner join u.roles r where r=:rol");
                query.setParameter("rol", rol);
                users.addAll((List<User>) query.getResultList());
            }

            return users;
        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public User getUser(long idUser) {
        try {
            return this.getById(idUser);
        } catch (Exception ex) {
            LOGGER.error("Error al recuperar Usuarios.", ex);
            return null;
        }
    }

    public User getUserByName(String username) {

        Query query = this.getEntityManager().createQuery("FROM User user WHERE user.username=:username");
        query.setParameter("username", username);
        query.setMaxResults(1);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }
}
