package cl.gechs.cursojava.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail.com>
 */
@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = "user_id"))
public class User implements Serializable {

    private static final long serialVersionUID = 2565328363629944996L;
    private Long id;
    private String name;
    private String email;
    private String username;
    private String password;
    private String address;
    private String phone;
    private boolean enabled;
    private List<UserRoleEnum> roles;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "user_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic(optional = false)
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic(optional = false)
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic(optional = false)
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic(optional = false)
    @Column(name = "password")
    public String getPassword() {
        return password;
    }
    
    @Basic(optional = false)
    @Column(name = "address")
    public String getAddress() {
        return address;
    }
  
    
    public void setAddress(String address) {
        this.address = address;
    }
    
    @Basic(optional = false)
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Basic(optional = false)
    @Column(name = "enabled")
    public boolean getEnabled() {
        return enabled;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    public List<UserRoleEnum> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRoleEnum> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return id + " | " + name + " | " + email + " | " + roles + " | " + password + " | " + address + " | " + phone;
    }

}
