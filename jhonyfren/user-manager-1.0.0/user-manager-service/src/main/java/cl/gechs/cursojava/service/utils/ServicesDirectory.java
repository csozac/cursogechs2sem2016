package cl.gechs.cursojava.service.utils;

import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.notifications.EmailNotificationService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public class ServicesDirectory {

    private static final Logger LOGGER = Logger.getLogger(ServicesDirectory.class);
    // services
    private static transient UsersService userService;
    private static transient EmailNotificationService emailNotificationService;

    private ServicesDirectory() {
    }

    public static UsersService getUserService() {
        try {
            if (userService == null) {
                userService = (UsersService) InitialContext
                        .doLookup("java:global/user-manager-ear/user-manager-service/UsersService!cl.gechs.cursojava.service.UsersService");
            }
        } catch (NamingException ex) {
            LOGGER.warn("Error injecting services: " + ex.getMessage(), ex);
        }
        return userService;
    }

    public static EmailNotificationService getEmailNotificationService() {
        try {
            if (emailNotificationService == null) {
                emailNotificationService = (EmailNotificationService) InitialContext
                        .doLookup("java:global/user-manager-ear/user-manager-service/EmailNotificationService!cl.gechs.cursojava.service.notifications.EmailNotificationService");
            }
        } catch (NamingException ex) {
            LOGGER.warn("Error injecting services: " + ex.getMessage(), ex);
        }
        return emailNotificationService;
    }
}
