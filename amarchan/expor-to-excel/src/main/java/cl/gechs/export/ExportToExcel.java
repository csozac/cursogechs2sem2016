/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gechs.export;

import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at profondos.com>
 */
public class ExportToExcel {

    private static final Logger LOGGER = Logger.getLogger(ExportToExcel.class);

    public static void main(String[] args) {
        LOGGER.info("Generando Archivo");
        GeneraExcel generaExcel = new GeneraExcel();
// Usar Path de archivo de sus m�quinas
        generaExcel.generaExcel("C:/Users/bmsoluciones/Downloads/Test.xls");
        LOGGER.info("Archivo Generado con �xito");
    }
}
