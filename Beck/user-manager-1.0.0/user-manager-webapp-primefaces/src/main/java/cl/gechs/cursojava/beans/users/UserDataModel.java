package cl.gechs.cursojava.beans.users;

import cl.gechs.cursojava.model.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Carlos Soza <carlos.soza@gmail.com>
 */
public class UserDataModel extends LazyDataModel<User> {

    private static final long serialVersionUID = -7350997936741504056L;
    private final transient UserController controller;

    public UserDataModel(UserController controller) {
        this.controller = controller;
    }

    @Override
    public User getRowData(String rowKey) {
        return this.controller.getUtilsBean().getUsersService().getUserByUsername(rowKey);
    }

    @Override
    public Object getRowKey(User user) {
        return user.getUsername();
    }

    @Override
    public List<User> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        List<User> data = new ArrayList<>();
        data.addAll(this.controller.getUtilsBean().getUsersService().getUsers());
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
        return data;
    }
}
