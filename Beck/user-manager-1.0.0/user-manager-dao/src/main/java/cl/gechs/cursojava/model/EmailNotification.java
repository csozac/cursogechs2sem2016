package cl.gechs.cursojava.model;

import cl.gechs.cursojava.commons.MailSender;
import cl.gechs.cursojava.enums.EmailNotificationType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@Entity
@Table(name = "email_notification", uniqueConstraints = @UniqueConstraint(columnNames = "email_notification_id"))
public class EmailNotification implements Serializable {

    private static final long serialVersionUID = -6529563161284301392L;
    private String emailNotificationId;
    private EmailNotificationType type;
    private MailSender.Type mailServerType;
    private String originEmailAddress;
    private String destinationEmailAddress;
    private String emailSubject;
    private String emailContent;
    private String updatedBy;
    private Date lastUpdate;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Basic(optional = true)
    @Column(name = "email_notification_id")
    public String getEmailNotificationId() {
        return emailNotificationId;
    }

    public void setEmailNotificationId(String emailNotificationId) {
        this.emailNotificationId = emailNotificationId;
    }

    @Basic(optional = true)
    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic(optional = true)
    @Column(name = "last_update")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Basic(optional = false)
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public EmailNotificationType getType() {
        return type;
    }

    public void setType(EmailNotificationType type) {
        this.type = type;
    }

    @Basic(optional = false)
    @Column(name = "mail_server_type")
    @Enumerated(EnumType.STRING)
    public MailSender.Type getMailServerType() {
        return mailServerType;
    }

    public void setMailServerType(MailSender.Type mailServerType) {
        this.mailServerType = mailServerType;
    }

    @Basic(optional = false)
    @Column(name = "email_origin_address")
    public String getOriginEmailAddress() {
        return originEmailAddress;
    }

    public void setOriginEmailAddress(String originEmailAddress) {
        this.originEmailAddress = originEmailAddress;
    }

    @Basic(optional = false)
    @Column(name = "email_destination_address")
    @Lob
    public String getDestinationEmailAddress() {
        return destinationEmailAddress;
    }

    public void setDestinationEmailAddress(String destinationEmailAddress) {
        this.destinationEmailAddress = destinationEmailAddress;
    }

    @Basic(optional = false)
    @Column(name = "email_subject")
    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    @Basic(optional = false)
    @Column(name = "email_content")
    @Lob
    public String getEmailContent() {
        return emailContent;
    }

    public void setEmailContent(String emailContent) {
        this.emailContent = emailContent;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EmailNotification) {
            return new EqualsBuilder().append(this.getEmailNotificationId(), ((EmailNotification) obj).getEmailNotificationId()).isEquals();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.emailNotificationId).toHashCode();
    }

    @Override
    public String toString() {
        return "Id: " + this.emailNotificationId + "| Type: " + this.type + "| OriginalEmailAddress:" + this.originEmailAddress
                + "| DestinationEmailAddress:" + this.destinationEmailAddress + "| MailServerType:" + this.mailServerType;
    }

}
