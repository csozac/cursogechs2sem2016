package cl.gechs.cursojava.enums;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail.com>
 */
public enum EmailNotificationType {

    REPORTE_AUTOMATICO, REPORTE_MANUAL;
}
