package cl.gechs.cursojava.commons;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public class MailAttachment {

    private String path;
    private boolean inline;

    public static MailAttachment make(String path, boolean inline) {
        return new MailAttachment(path, inline);
    }

    private MailAttachment(String path, boolean inline) {
        this.path = path;
        this.inline = inline;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isInline() {
        return inline;
    }

    public void setInline(boolean inline) {
        this.inline = inline;
    }

    @Override
    public String toString() {
        return "Path: " + path + " | inline: " + inline;
    }

}
