package cl.gechs.cursojava.commons;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public abstract class MailSender implements Callable<Boolean> {

    protected static final String CANNOT_SEND_EMAIL = "Cannot send Email: ";
    protected static final String SMTP = "smtp";
    protected static final String MIXED = "mixed";
    protected static final String ENCODING = "UTF-8";
    protected static final String CONTENT_TYPE = "Content-Type";
    protected static final String TEXT_HTML_CHARSET_UTF_8 = "text/html; charset=UTF-8";
    // email data
    protected final String[] emails;
    protected final String subject;
    protected final String body;
    protected final String subjectPrefix;
    protected final List<MailAttachment> attachments;

    public MailSender(String[] emails, String subjectPrefix, String subject, String body, List<MailAttachment> attachments) {
        if (emails != null) {
            this.emails = emails.clone();
        } else {
            this.emails = new String[0];

        }
        this.subjectPrefix = subjectPrefix;
        this.subject = subject;
        this.body = body;

        if (attachments != null) {
            this.attachments = new ArrayList<>();
            this.attachments.addAll(attachments);
        } else {
            this.attachments = new ArrayList<>();
        }
    }

    public String getSubjectPrefix() {
        return subjectPrefix;
    }

    public String[] getEmails() {
        return emails;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public List<MailAttachment> getAttachments() {
        return attachments;
    }

    @Override
    public abstract Boolean call();

    public enum Type {

        GMAIL, EXCHANGE;
    }
    
}
