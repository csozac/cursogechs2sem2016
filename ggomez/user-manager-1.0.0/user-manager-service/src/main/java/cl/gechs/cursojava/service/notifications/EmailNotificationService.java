package cl.gechs.cursojava.service.notifications;

import cl.gechs.cursojava.dao.jpa.EmailNotificationDAO;
import cl.gechs.cursojava.dao.jpa.EmailNotificationDocumentDAO;
import cl.gechs.cursojava.enums.EmailNotificationType;
import cl.gechs.cursojava.model.EmailNotification;
import cl.gechs.cursojava.model.EmailNotificationDocument;
import java.util.List;

import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@Startup
@Singleton
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@AccessTimeout(value = 15000)
public class EmailNotificationService implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(EmailNotificationService.class);
    // CONSTANTS
    private static final String SERVICE_NAME = "EmailNotificationService_";
    public static final String LINE_SEPARATOR_01 = "-------------------------------------------------------------------------";
    public static final String LINE_SEPARATOR_02 = "-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-";
    public static final String LINE_SEPARATOR_03 = "*************************************************************************";
    public static final String SAVE_ERROR = "SAVE DATABASE ERROR";
    public static final String UPDATE_ERROR = "UPDATE DATABASE ERROR";
    public static final String DELETE_ERROR = "DELETE DATABASE ERROR";
    public static final String ENTITY_NOT_FOUND_ERROR = "ENTITY NOT FOUND";
    public static final String ENTITY_ALREADY_REGISTERED_ERROR = "ENTITY ALREADY REGISTERED";
    public static final String CANNOT_SAVE_ERROR = "Cannot Save ";
    public static final String CANNOT_UPDATE_ERROR = "Cannot Update ";
    public static final String CANNOT_DELETE_ERROR = "Cannot Delete ";
    // CONSTANTS
    protected static final String BECAUSE_IT_DOESN_T_EXISTS = " because it doesn't exists";

    // DAOs
    @EJB
    private EmailNotificationDAO emailNotificationDAO;
    @EJB
    private EmailNotificationDocumentDAO emailNotificationDocumentDAO;

    @javax.annotation.PostConstruct
    public void start() {

        LOGGER.info(LINE_SEPARATOR_02);
        LOGGER.info("Staring Service: " + SERVICE_NAME);
        LOGGER.info(LINE_SEPARATOR_01);
    }

    @javax.annotation.PreDestroy
    public void stop() {
        LOGGER.info(LINE_SEPARATOR_02);
        LOGGER.info("Stoping Service: " + SERVICE_NAME);
        LOGGER.info(LINE_SEPARATOR_01);
    }

    /* EMAIL NOTIFICATION  MANAGEMENT */
    @Lock(LockType.WRITE)
    public boolean saveEmailNotification(EmailNotification emailNotification, List<EmailNotificationDocument> emailNotificationDocuments, String requestedBy) {
        try {
            emailNotification.setUpdatedBy(requestedBy);
            emailNotification.setLastUpdate(new Date());
            if (this.emailNotificationDAO.save(emailNotification)) {
                // save documents
                for (EmailNotificationDocument emailND : emailNotificationDocuments) {
                    emailND.setEmailNotification(emailNotification);
                    emailND.setUpdatedBy(requestedBy);
                    emailND.setLastUpdate(new Date());
                    if (!this.emailNotificationDocumentDAO.save(emailND)) {
                        LOGGER.warn("Cannot Save EmailNotification Document: " + emailND);
                    }
                }
                return true;
            } else {

                return false;
            }
        } catch (Exception ex) {
            LOGGER.warn("Cannot Save EmailNotification : " + emailNotification + ". Error: " + ex.getMessage(), ex);
            return false;
        }
    }

    @Lock(LockType.WRITE)
    public boolean deleteEmailNotification(EmailNotification emailNotification, String requestedBy) {
        try {
            // get email notification
            EmailNotification eenotification = this.emailNotificationDAO.getEmailNotification(emailNotification.getEmailNotificationId());
            if (eenotification != null) {

                emailNotification.setUpdatedBy(requestedBy);
                emailNotification.setLastUpdate(new Date());

                // Deleting related documents
                List<EmailNotificationDocument> emailNotificationDocuments = this.emailNotificationDocumentDAO.getEmailNotificationDocuments(emailNotification.getEmailNotificationId());
                for (EmailNotificationDocument en : emailNotificationDocuments) {
                    File df = new File(en.getFilePath());
                    if (df.exists()) {
                        try {
                            Files.delete(Paths.get(en.getFilePath()));
                        } catch (IOException ex) {
                            LOGGER.warn("Cannot Delete EmailNotification Document. Error: " + ex.getMessage() + " Email Notification Document" + en, ex);
                        }
                    }
                    if (!this.emailNotificationDocumentDAO.delete(en)) {
                        LOGGER.warn("Cannot Delete EmailNotification Document: " + en);
                    }
                }
                if (this.emailNotificationDAO.delete(emailNotification)) {
                    return true;
                } else {
                    LOGGER.warn("Cannot Delete EmailNotification: " + emailNotification);
                    return false;
                }
            } else {
                LOGGER.error(
                        DELETE_ERROR + ". " + ENTITY_NOT_FOUND_ERROR + ": " + emailNotification.getEmailNotificationId());
                return false;
            }
        } catch (Exception ex) {
            LOGGER.warn("Cannot Delete EmailNotification : " + emailNotification + ". Error: " + ex.getMessage(), ex);
            return false;
        }
    }

    @Lock(LockType.READ)
    public List<EmailNotification> getEmailNotifications(List<EmailNotificationType> types) {
        return this.emailNotificationDAO.getEmailNotifications(types);
    }
    
    @Lock(LockType.READ)
    public List<EmailNotification> getEmailNotifications() {
        return this.emailNotificationDAO.getEmailNotifications();
    }

    @Lock(LockType.READ)
    public EmailNotification getEmailNotification(String emailNotificationId) {
        return this.emailNotificationDAO.getEmailNotification(emailNotificationId);
    }

    /* EMAIL NOTIFICATION DOCUMENTS MANAGEMENT */
    @Lock(LockType.READ)
    public List<EmailNotificationDocument> getEmailNotificationDocuments(String emailNotificationId) {
        return this.emailNotificationDocumentDAO.getEmailNotificationDocuments(emailNotificationId);
    }

    @Lock(LockType.READ)
    public EmailNotificationDocument getEmailNotificationDocument(String documentId) {
        return this.emailNotificationDocumentDAO.getEmailNotificationDocument(documentId);
    }

    @Lock(LockType.WRITE)
    public boolean deleteEmailNotificationDocument(String documentId) {
        EmailNotificationDocument document = this.emailNotificationDocumentDAO.getEmailNotificationDocument(documentId);
        if (document != null) {
            return this.emailNotificationDocumentDAO.delete(document);
        }
        return false;
    }
}
