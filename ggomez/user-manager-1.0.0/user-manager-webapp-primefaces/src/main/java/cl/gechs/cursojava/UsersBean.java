package cl.gechs.cursojava;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

@ManagedBean(name = "usersBean")
@ViewScoped
public class UsersBean implements Serializable {

    private static final UsersService USERS_SERVICE;
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(UsersBean.class);

    static {
        USERS_SERVICE = ServicesDirectory.getUserService();
    }

    private List<User> users;

    @PostConstruct
    public void init() {
        users = USERS_SERVICE.getUsers();
        LOGGER.info("List Users Size: " + users.size());
    }

    public List<User> getUsers() {
        return users;
    }
}
