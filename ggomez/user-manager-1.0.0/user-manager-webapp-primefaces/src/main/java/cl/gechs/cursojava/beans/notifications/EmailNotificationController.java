package cl.gechs.cursojava.beans.notifications;

import cl.gechs.cursojava.enums.EmailNotificationType;
import cl.gechs.cursojava.model.EmailNotification;
import cl.gechs.cursojava.beans.LoginBean;
import cl.gechs.cursojava.beans.UtilsBean;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@ManagedBean(name = "emailNotificationController")
@SessionScoped
public class EmailNotificationController implements Serializable {

    private static final long serialVersionUID = 4357704426773030376L;
    // BEANS
    @ManagedProperty("#{utilsBean}")
    private UtilsBean utilsBean;
    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;
    // FILTERS
    private String organizationId;
    private EmailNotificationType emailNotificationType;
    // DATA
    private LazyDataModel<EmailNotification> emailNotifications;
    private EmailNotification emailNotification;

    @PostConstruct
    public void init() {
        this.emailNotifications = new EmailNotificationModel(this);
    }

    public void resetFilters() {
        this.organizationId = null;
        this.emailNotificationType = null;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public EmailNotificationType getEmailNotificationType() {
        return emailNotificationType;
    }

    public void setEmailNotificationType(EmailNotificationType emailNotificationType) {
        this.emailNotificationType = emailNotificationType;
    }

    public LazyDataModel<EmailNotification> getEmailNotifications() {
        return emailNotifications;
    }

    public void setEmailNotifications(LazyDataModel<EmailNotification> emailNotifications) {
        this.emailNotifications = emailNotifications;
    }

    public EmailNotification getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(EmailNotification emailNotification) {
        this.emailNotification = emailNotification;
    }

    public UtilsBean getUtilsBean() {
        return utilsBean;
    }

    public void setUtilsBean(UtilsBean utilsBean) {
        this.utilsBean = utilsBean;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public EmailNotificationType[] getEmailNotificationTypes() {
        return EmailNotificationType.values();
    }
}
