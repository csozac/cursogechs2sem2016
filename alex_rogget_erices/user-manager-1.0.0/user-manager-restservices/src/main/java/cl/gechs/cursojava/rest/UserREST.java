package cl.gechs.cursojava.rest;

import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail.com>
 */
@Path("/user")
public class UserREST {

    private static final Logger LOGGER = Logger.getLogger(UserREST.class);
    private static final Gson GSON = new GsonBuilder().create();
    private static final UsersService USERS_SERVICE;

    static {
        USERS_SERVICE = ServicesDirectory.getUserService();
    }

    @GET
    @Path("userList")
    @Produces({"application/json"})
    public String getUsers() {
        List<User> users = USERS_SERVICE.getUsers();
        LOGGER.info("Return users List Size: " + users.size());
        return GSON.toJson(users);
    }

    @GET
    @Path("/obtener/{userId}")
    @Produces("application/json")
    public String obtenerUsuario(@PathParam("userId") Long userId) {
        User u = USERS_SERVICE.getUser(userId);
        return GSON.toJson(u);
    }
}
