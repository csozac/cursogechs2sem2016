package cl.gechs.cursojava.beans;

import cl.gechs.cursojava.service.UsersService;
import cl.gechs.cursojava.service.notifications.EmailNotificationService;
import cl.gechs.cursojava.service.utils.ServicesDirectory;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@ManagedBean(name = "utilsBean")
@SessionScoped
public class UtilsBean implements Serializable {

    private static final long serialVersionUID = -8176325958172860318L;

    public String getTimeZone() {
        return TimeZone.getDefault().getID();
    }

    public UsersService getUsersService() {
        return ServicesDirectory.getUserService();
    }

    public EmailNotificationService getEmailNotificationService() {
        return ServicesDirectory.getEmailNotificationService();
    }

    public Locale getLocale() {
        return new Locale.Builder().setLanguage("es").setRegion("CL").build();
    }

    public String getServerIp() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public String getServerName() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }

}
