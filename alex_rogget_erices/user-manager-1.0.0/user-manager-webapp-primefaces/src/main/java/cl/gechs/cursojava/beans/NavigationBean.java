package cl.gechs.cursojava.beans;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@ManagedBean(name = "navigationBean")
@SessionScoped
public class NavigationBean implements Serializable {

    private static final long serialVersionUID = -6276136412280762262L;
    private static final Logger LOGGER = Logger.getLogger(NavigationBean.class);
    // BEANS
    @ManagedProperty("#{utilsBean}")
    private UtilsBean utilsBean;
    // DATA
    private String currentPage = "users/management";
    private String currentPagePath = "/user-manager-webapp-primefaces/pages/" + currentPage + ".xhtml";

    public String getCurrentPage() {
        return currentPage;
    }

    public String getCurrentPagePath() {
        LOGGER.info("GetCurrent Path: " + currentPage);
        return currentPagePath;
    }

    public void setCurrentPage(String currentPage, boolean redirect) throws IOException {
        this.currentPagePath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/" + currentPage + ".xhtml";
        this.currentPage = currentPage;

        LOGGER.info("SetCurrent Path: " + currentPagePath);
        if (redirect) {
            FacesContext.getCurrentInstance().getExternalContext().redirect(this.currentPagePath);
        }
    }

    public String getLoginPath() {
        return "/login.jsf";
    }

    public String getHomePath() {
        return "/pages/users/management.jsf";
    }

    public void redirectToLogin() throws IOException {
        LOGGER.info("Redirect To Login!");
        FacesContext.getCurrentInstance().getExternalContext()
                .redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + this.getLoginPath());
    }

    public void redirectToHome() throws IOException {
        LOGGER.info("Redirect To Home!");
        FacesContext.getCurrentInstance().getExternalContext()
                .redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + this.getHomePath());
    }

    public UtilsBean getUtilsBean() {
        return utilsBean;
    }

    public void setUtilsBean(UtilsBean utilsBean) {
        this.utilsBean = utilsBean;
    }

}
