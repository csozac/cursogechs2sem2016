package cl.gechs.cursojava;

import cl.gechs.cursojava.dao.jpa.BaseDAO;
import cl.gechs.cursojava.dao.jpa.UserDAO;
import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRoleEnum;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail.com>
 */
@RunWith(Arquillian.class)
public class UserDAOTest {

    private static final Logger LOGGER = Logger.getLogger(UserDAOTest.class);

    @Inject
    private UserDAO userDAO;

    @Deployment(name = "user-manager-database", order = 1, testable = true)
    public static WebArchive createDatabaseDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "user-manager-database.war")
                .addClass(UserDAO.class)
                .addClass(BaseDAO.class)
                .addClass(UserRoleEnum.class)
                .addClass(User.class)
                .addAsLibraries(files)
                .addAsWebInfResource("META-INF/jboss-deployment-structure.xml")
                .addAsResource("persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));

        return war;
    }

    @Test
    @OperateOnDeployment("user-manager-database")
    @InSequence(1)
    public void testInjections() throws Exception {
        Assert.assertNotNull(this.userDAO);
    }

    @Test
    @OperateOnDeployment("user-manager-database")
    @InSequence(2)
    public void testUserDAOAddUpdate() {
        Assert.assertNotNull(this.userDAO);

        User u = new User();
        u.setName("Carlos Soza");
        u.setPassword("csozac");
        u.setEmail("carlos.soza@gmail.com");
        u.setUsername("csozac");
        u.setEnabled(true);
        List<UserRoleEnum> roles = new ArrayList<>();
        roles.add(UserRoleEnum.PARTICIPANTE);
        roles.add(UserRoleEnum.ADMINISTRADOR);
        u.setRoles(roles);

        //insert
        Assert.assertTrue("Error al guardar el usuario", this.userDAO.save(u));
        LOGGER.info("Usuario Insertado en BD: " + this.userDAO.getById(u.getId()));

        //update
        u.setEnabled(Boolean.FALSE);
        u.setPassword(encryptPassword("12345"));
        Assert.assertTrue(this.userDAO.update(u));
        LOGGER.info("Usuario Actualiazdo en BD: " + this.userDAO.getById(u.getId()));

        //delete
        //Assert.assertTrue(this.userDAO.delete(user));
        //Assert.assertTrue(this.userRolDAO.delete(ur));
    }

    private String encryptPassword(String password) {
        return new String(Base64.encodeBase64(DigestUtils.md5(password)));
    }

}
