package cl.gechs.cursojava.dao.jpa;

import cl.gechs.cursojava.model.EmailNotificationDocument;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@javax.ejb.Stateless
@javax.ejb.LocalBean
public class EmailNotificationDocumentDAO extends BaseDAO<EmailNotificationDocument> {

    private static final Logger LOGGER = Logger.getLogger(EmailNotificationDocumentDAO.class);

    @Override
    public Class<EmailNotificationDocument> getObjectClass() {
        return EmailNotificationDocument.class;
    }

    public EmailNotificationDocument getEmailNotificationDocument(String documentId) {
        Query query = this.getEntityManager().createQuery("FROM EmailNotificationDocument where documentId=:documentId");
        query.setParameter("documentId", documentId);
        try {
            return (EmailNotificationDocument) query.getSingleResult();
        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

    public List<EmailNotificationDocument> getEmailNotificationDocuments(String emailNotificationId) {
        try {
            Query query = this.getEntityManager().createQuery("FROM EmailNotificationDocument e where e.emailNotification.emailNotificationId=:emailNotificationId ");
            query.setParameter("emailNotificationId", emailNotificationId);
            return query.getResultList();

        } catch (NoResultException ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        } catch (Exception ex) {
            LOGGER.error("ERROR DAO: ", ex);
            return null;
        }
    }

}
