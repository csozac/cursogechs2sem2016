package cl.gechs.cursojava.model;

import cl.gechs.cursojava.model.base.BasicDocument;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
@Entity
@Table(name = "email_notification_document", uniqueConstraints = @UniqueConstraint(columnNames = "document_id"))
public class EmailNotificationDocument extends BasicDocument {

    private static final long serialVersionUID = -388628159173927164L;
    private String documentId;
    private String documentName;
    private EmailNotification emailNotification;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Basic(optional = true)
    @Column(name = "document_id")
    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    @Basic(optional = false)
    @Column(name = "document_name")
    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "email_notification_id", referencedColumnName = "email_notification_id")
    public EmailNotification getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(EmailNotification emailNotification) {
        this.emailNotification = emailNotification;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.documentId).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EmailNotificationDocument) {
            return new EqualsBuilder().append(this.getDocumentId(), ((EmailNotificationDocument) obj).getDocumentId()).append(this.getDocumentName(), ((EmailNotificationDocument) obj).getDocumentName()).isEquals();
        }
        return false;
    }

    @Override
    public String toString() {
        return "Id: " + this.documentId + "| type: " + this.getType() + "| FilePath: " + this.getFilePath();
    }

}
