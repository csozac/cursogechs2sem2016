package cl.gechs.cursojava.service;

import cl.gechs.cursojava.dao.jpa.UserDAO;
import cl.gechs.cursojava.model.User;
import cl.gechs.cursojava.model.UserRoleEnum;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail dot com>
 */
@Startup
@Singleton
@LocalBean
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class UsersService implements UsersServiceMXBean {

    private static final Logger LOGGER = Logger.getLogger(UsersService.class);
    private static final SecureRandom RANDOM = new SecureRandom();
    //DAOS
    @EJB
    private UserDAO userDAO;
    //MBEAN
    private static MBeanServer platformMBeanServer;
    private static ObjectName objectName = null;

    @javax.annotation.PostConstruct
    public void start() {
        LOGGER.info("-------------------------------------------------------------------------");
        LOGGER.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        LOGGER.info("Starting Users Service");

        //register mbean
        try {
            objectName = new ObjectName("user-manager:services=" + this.getClass().getName());
            platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            platformMBeanServer.registerMBean(this, objectName);
        } catch (MalformedObjectNameException | InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException e) {
            throw new IllegalStateException("Problem during registration of Monitoring into JMX:" + e);
        }

        LOGGER.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        LOGGER.info("-------------------------------------------------------------------------");
    }

    @javax.annotation.PreDestroy
    public void stop() {
        LOGGER.info("-------------------------------------------------------------------------");
        LOGGER.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        LOGGER.info("Ending Users Service");

        //unregister mbean
        try {
            platformMBeanServer.unregisterMBean(objectName);
        } catch (InstanceNotFoundException | MBeanRegistrationException e) {
            throw new IllegalStateException("Problem during unregistration of Monitoring into JMX:" + e);
        }

        LOGGER.info("-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-");
        LOGGER.info("-------------------------------------------------------------------------");
    }

    @Lock(LockType.WRITE)
    public boolean saveUser(User user) {
        try {
            if (userDAO.save(user)) {
                return true;
            } else {
                LOGGER.error("Cannot save User " + user + ". Database error");
            }

        } catch (Exception ex) {
            LOGGER.error("Cannot save User " + user + ": ", ex);
        }
        return false;
    }

    @Lock(LockType.WRITE)
    public boolean updateUser(User user) {
        try {
            //check user id
            User euser = userDAO.getUser(user.getId());
            if (euser != null) {
                if (userDAO.update(user)) {
                    return true;
                } else {
                    LOGGER.error("Cannot update User " + user + ". Database error");
                }
            } else {
                LOGGER.error("Cannot update User " + user + ". UserId not registered");
            }
        } catch (Exception ex) {
            LOGGER.error("Cannot update User " + user + ": ", ex);
        }
        return false;
    }

    @Lock(LockType.WRITE)
    public boolean deleteUser(User user) {
        try {
            //check user id
            User euser = userDAO.getUser(user.getId());
            if (euser != null) {
                if (userDAO.delete(user)) {
                    return true;
                } else {
                    LOGGER.error("Cannot delete User " + user + ". Database error");
                }
            } else {
                LOGGER.error("Cannot delete User " + user + ". UserId not registered");
            }
        } catch (Exception ex) {
            LOGGER.error("Cannot delete User " + user + ": ", ex);
        }
        return false;
    }

    @Lock(LockType.READ)
    public String generatePassword(Long userId) {
        //get user
        User user = userDAO.getUser(userId);
        if (user != null) {
            //generate password
            String password = this.createPassword();
            //encrypt password and set to the user
            user.setPassword(this.encryptPassword(password));
            if (userDAO.update(user)) {
                return password;
            } else {
                LOGGER.error("Cannot generarte password to User " + userId + ". Database error");
            }
        } else {
            LOGGER.error("Cannot generarte password to User " + userId + ". Entity not found");
        }
        return null;
    }

    @Lock(LockType.READ)
    public User getUser(Long userId) {
        return userDAO.getUser(userId);
    }

    @Lock(LockType.READ)
    public List<User> getUsers() {
        return userDAO.getUsers();
    }

    @Lock(LockType.READ)
    public List<User> getUsers(List<UserRoleEnum> roles) {
        return userDAO.getUsers(roles);
    }

    @Lock(LockType.READ)
    public User getUserByUsername(String userName) {
        return userDAO.getUserByName(userName);
    }

    @Lock(LockType.READ)
    public List<UserRoleEnum> getUserRoles(String username) {
        return userDAO.getUserRoles(username);
    }

    @Override
    @Lock(LockType.READ)
    public String encryptUserPassword(String password) {
        return encryptPassword(password);
    }

    private String createPassword() {
        String password = new BigInteger(130, RANDOM).toString(32);
        return password.substring(0, 10);
    }

    private String encryptPassword(String password) {
        return new String(Base64.encodeBase64(DigestUtils.md5(password)));
    }

    public boolean isUserInRoles(String userName, List<UserRoleEnum> roles) {
        List<User> users = this.getUsers(roles);
        return users != null && !users.isEmpty();
    }

    @Override
    public String usersList() {
        List<User> users = this.getUsers();
        String userList = "";
        for (User u : users) {
            userList += u.toString() + "\n";
        }
        return userList;
    }
}
