package cl.gechs.cursojava.service;

/**
 *
 * @author Carlos Soza C <carlos.soza at gnail dot com>
 */
public interface UsersServiceMXBean {

    public String encryptUserPassword(String password);

    public String usersList();
}
