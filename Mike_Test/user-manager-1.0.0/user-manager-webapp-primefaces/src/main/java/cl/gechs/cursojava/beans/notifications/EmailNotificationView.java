package cl.gechs.cursojava.beans.notifications;

import cl.gechs.cursojava.commons.MailAttachment;
import cl.gechs.cursojava.commons.MailSender;
import cl.gechs.cursojava.commons.MailSenderConfig;
import cl.gechs.cursojava.commons.MailSenderFactory;
import cl.gechs.cursojava.enums.EmailNotificationType;
import cl.gechs.cursojava.model.EmailNotification;
import cl.gechs.cursojava.model.EmailNotificationDocument;
import cl.gechs.cursojava.beans.LoginBean;
import cl.gechs.cursojava.beans.UtilsBean;
import cl.gechs.cursojava.commons.Attachment;
import cl.gechs.cursojava.commons.BaseController;
import cl.gechs.cursojava.commons.RandomUtils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail.com>
 */
@ManagedBean(name = "emailNotificationView")
@ViewScoped
public class EmailNotificationView extends BaseController implements Serializable {

    private static final long serialVersionUID = -6669990565378644861L;
    private static final Logger LOGGER = Logger.getLogger(EmailNotificationView.class);
    private static final String FACTORY_EMAIL = "factory@gmail.com";
    private static final Integer FILE_NAME_RANDOM_SIZE = 10;
    private static final String TEMP_DIR = "/tmp";
    private static final String NOTIFICATIONS_DIR;
    protected static final String HTML_EDITOR_TOOLBAR = "[['Source','Preview','Print'],['Cut','Copy','Paste','PasteText','PasteFromWord','Undo','Redo'],"
            + "['Find','Replace','SelectAll'],['Bold','Italic','Underline','Strike','Subscript','Superscript','RemoveFormat']"
            + ",['Outdent','Indent','Blockquote','CreateDiv','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','BidiLtr','BidiRtl','NumberedList','BulletedList']"
            + ",['Link','Unlink','Anchor'],['Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe','InsertPre']"
            + ",['Styles','Format','Font','FontSize'],['TextColor','BGColor'],['UIColor','Maximize','ShowBlocks'],['About']]";

    static {
        NOTIFICATIONS_DIR = System.getProperty("jboss.home.dir") + "/standalone/resources/notifications";
    }

    // BEANS
    @ManagedProperty("#{utilsBean}")
    private UtilsBean utilsBean;
    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;
    //DATA
    private EmailNotification emailNotification;
    private EmailNotification selectedEmailNotification;
    private List<EmailNotificationDocument> selectedEmailNotificationDocuments;
    private transient List<EmailNotificationDocument> addEmailNotificationDocuments;
    private transient List<EmailNotificationDocument> emailNotificationDocuments;
    private List<String> fileNames;
    private transient StreamedContent file;

    @PostConstruct
    public void init() {
        this.initEmailNotification();
        this.addEmailNotificationDocuments = new ArrayList<>();
        this.emailNotificationDocuments = new ArrayList<>();
        this.fileNames = new ArrayList<>();
    }

    public void reinitDocument() {
        this.fileNames = new ArrayList<>();
        this.addEmailNotificationDocuments = new ArrayList<>();
    }

    public void initEmailNotification() {
        this.emailNotification = new EmailNotification();
        this.emailNotification.setOriginEmailAddress(FACTORY_EMAIL);
    }

    public void reinit() {
        this.initEmailNotification();
        this.addEmailNotificationDocuments = new ArrayList<>();
        this.emailNotificationDocuments = new ArrayList<>();
        this.fileNames = new ArrayList<>();
    }

    public EmailNotification getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(EmailNotification emailNotification) {
        this.emailNotification = emailNotification;
    }

    public List<EmailNotificationDocument> getEmailNotificationDocuments() {
        return emailNotificationDocuments;
    }

    public void setEmailNotificationDocuments(List<EmailNotificationDocument> emailNotificationDocuments) {
        this.emailNotificationDocuments = emailNotificationDocuments;
    }

    public List<EmailNotificationDocument> getAddEmailNotificationDocuments() {
        return addEmailNotificationDocuments;
    }

    public void setAddEmailNotificationDocuments(List<EmailNotificationDocument> addEmailNotificationDocuments) {
        this.addEmailNotificationDocuments = addEmailNotificationDocuments;
    }

    public EmailNotification getSelectedEmailNotification() {
        return selectedEmailNotification;
    }

    public void setSelectedEmailNotification(EmailNotification selectedEmailNotification) {
        this.selectedEmailNotification = selectedEmailNotification;
        List<EmailNotificationDocument> eNDocuments = this.getUtilsBean().getEmailNotificationService().getEmailNotificationDocuments(selectedEmailNotification.getEmailNotificationId());
        if (eNDocuments != null) {
            this.setSelectedEmailNotificationDocuments(eNDocuments);
        }
    }

    public List<EmailNotificationDocument> getSelectedEmailNotificationDocuments() {
        return selectedEmailNotificationDocuments;
    }

    public void setSelectedEmailNotificationDocuments(List<EmailNotificationDocument> selectedEmailNotificationDocuments) {
        this.selectedEmailNotificationDocuments = selectedEmailNotificationDocuments;
    }

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public UtilsBean getUtilsBean() {
        return this.utilsBean;
    }

    public LoginBean getLoginBean() {
        return this.loginBean;
    }

    public void setUtilsBean(UtilsBean utilsBean) {
        this.utilsBean = utilsBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public void doDelete(EmailNotification emailNotification) {
        LOGGER.info("Deleting Notification: " + emailNotification.getEmailNotificationId());
        if (this.getUtilsBean().getEmailNotificationService().deleteEmailNotification(emailNotification, this.loginBean.getUsername())) {
            this.generateMainInfoMessage("Delete", " Notificacion Borrada con Èxito");
        } else {
            this.generateMainErrorMessage("Error", " Error al eliminar Notificacion");
        }
    }

    public boolean sendNotification(EmailNotification emailNotification) {
        try {
            LOGGER.info("Send Notification: " + emailNotification.getEmailNotificationId());
            List<EmailNotificationDocument> emNDocuments = this.getUtilsBean().getEmailNotificationService().getEmailNotificationDocuments(emailNotification.getEmailNotificationId());
            List<String> attachments = new ArrayList<>();
            if (emNDocuments != null && !emNDocuments.isEmpty()) {
                for (EmailNotificationDocument matt : emNDocuments) {
                    File f = new File(matt.getFilePath());
                    if (f.exists()) {
                        String ndFilename = TEMP_DIR + "/" + matt.getDocumentName();
                        LOGGER.info("Moving File\nFROM=" + matt.getFilePath() + "\nTO=" + ndFilename);
                        CopyOption[] options = new CopyOption[]{StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES};
                        Files.copy(Paths.get(matt.getFilePath()), Paths.get(ndFilename), options);
                        attachments.add(ndFilename);
                    }
                }
            }
            this.sendEmail(emailNotification.getDestinationEmailAddress().split(","), emailNotification.getEmailSubject(), emailNotification.getEmailContent(), attachments.toArray(new String[attachments.size()]), emailNotification.getType(), emailNotification.getMailServerType());
            this.generateMainInfoMessage("OK", "Notificacion Enviada con Éxito");
        } catch (Exception ex) {
            LOGGER.error("Cannot send notifiation: " + ex.getMessage(), ex);
            this.generateMainErrorMessage("Error", " Error al enviar notificacion");
        }
        return true;
    }

    public void doSaveAndSend() {
        LOGGER.info("Send and Save Notification");
        this.doSave();
        this.sendNotification(emailNotification);
    }

    public void doSave() {
        LOGGER.info("Saving Notification");
        try {
            for (EmailNotificationDocument matt : emailNotificationDocuments) {
                // move file to the right folder
                String fileExtension = this.getFileExtension(new File(matt.getFilePath()));
                fileExtension += "." + fileExtension;
                String ndFilename = NOTIFICATIONS_DIR + "/" + RandomUtils.generateRandomID(FILE_NAME_RANDOM_SIZE).toUpperCase() + fileExtension;
                LOGGER.info("Moving File\nFROM=" + matt.getFilePath() + "\nTO=" + ndFilename);
                CopyOption[] options = new CopyOption[]{StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES};
                Files.copy(Paths.get(matt.getFilePath()), Paths.get(ndFilename), options);
                matt.setFilePath(ndFilename);
                matt.setType("Attachment");
            }
            this.emailNotification.setMailServerType(MailSender.Type.GMAIL);
            if (this.getUtilsBean().getEmailNotificationService().saveEmailNotification(emailNotification, emailNotificationDocuments, this.loginBean.getUsername())) {
                this.generateMainInfoMessage("NOtificación", "Notificacion Guardada");
            } else {
                this.generateMainErrorMessage("Error", "No se pudo guardar la Notificación");
            }
        } catch (Exception ex) {
            LOGGER.error("Cannot save notifiation: " + ex.getMessage(), ex);
            this.generateMainErrorMessage("Error", "No se pudo guardar la Notificación");
        }
    }

    public void onAddDocument() {
        if (this.addEmailNotificationDocuments != null && !this.addEmailNotificationDocuments.isEmpty()) {
            for (EmailNotificationDocument ed : this.addEmailNotificationDocuments) {
                if (this.emailNotificationDocuments.contains(ed)) {
                    this.generateErrorMessage("Notificacion", "Notificación Duplicada ");
                } else {
                    this.emailNotificationDocuments.add(ed);
                }
            }
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            // write file to temp
            UploadedFile uploadFile = event.getFile();
            LOGGER.info("Writing File to Temp: " + uploadFile.getFileName());
            String location = TEMP_DIR + "/"
                    + RandomUtils.generateRandomID(20).toUpperCase() + "." + FilenameUtils.getExtension(uploadFile.getFileName());
            uploadFile.write(location);
            EmailNotificationDocument emailNotificationDocument = new EmailNotificationDocument();
            emailNotificationDocument.setFilePath(location);
            emailNotificationDocument.setDocumentName(uploadFile.getFileName());
            LOGGER.debug("New file Uploaded: " + location);
            this.addEmailNotificationDocuments.add(emailNotificationDocument);
            // set uploaded filename
            this.fileNames.add(uploadFile.getFileName());
        } catch (Exception ex) {
            LOGGER.error("Cannot write File: ", ex);
        }
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public void prepareFile(String location, String fileName) throws FileNotFoundException {
        Attachment attachment = Attachment.make(location);
        InputStream stream = new ByteArrayInputStream(attachment.getData());
        this.file = new DefaultStreamedContent(stream, attachment.getType(), fileName);
    }

    public MailSender.Type[] getMailServerTypes() {
        return MailSender.Type.values();
    }

    public String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public void sendEmail(String[] emails, String subject, String body, String[] attachments, EmailNotificationType notificationType, MailSender.Type mailSenderType) throws InterruptedException, ExecutionException {
        // send emails in a new thread
        MailSender mailSender = null;
        switch (mailSenderType) {
            case GMAIL:
                List<MailAttachment> mailAttachments = new ArrayList<>();
                for (String a : attachments) {
                    mailAttachments.add(MailAttachment.make(a, false));
                }
                MailSenderConfig mscGmail = MailSenderConfig.make("factory@gmail.com", "", "smtp.gmail.com", "587", "true", "true", "Factory", "factory@gmail.com", true);
                mailSender = MailSenderFactory.makeMailSender(emails, "GECHS", subject, body, mailAttachments,
                        mailSenderType, mscGmail);
                break;
            case EXCHANGE:
                /*MailSenderConfig mscExchange = MailSenderConfig.make(mailServerType.getUsername(), mailServerType.getPassword(), mailServerType.getHost(),
                        mailServerType.getFromName(), mailServerType.getFromEmail());
                mailSender = MailSenderFactory.makeMailSender(emails, PROPERTIES.getGeneral().getInstanceEnvironment().name(), subject, body, attachments,
                        mailSenderType, mscExchange);*/
                break;
            default:
                LOGGER.error("Error creating mail sender, type not recognized: " + mailSenderType);
                break;
        }
        if (mailSender != null) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Boolean> sent = executor.submit(mailSender);
            LOGGER.info("Enviado: " + sent.get());
            executor.shutdownNow();
        } else {
            LOGGER.warn("Cannot send Email because cannot create MailSender. See before logs");
        }
    }

    public String getToolbarConfig() {
        return HTML_EDITOR_TOOLBAR;
    }
}
