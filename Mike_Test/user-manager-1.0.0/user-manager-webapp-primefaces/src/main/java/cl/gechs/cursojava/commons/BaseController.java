package cl.gechs.cursojava.commons;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Carlos Soza C <carlos.soza at gmail.com>
 */
public class BaseController implements Serializable {

    private static final long serialVersionUID = 5795760214451864758L;
    protected static final String MAIN_MESSAGES_KEY = "main_messages";

    private void generateMainMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(MAIN_MESSAGES_KEY, new FacesMessage(severity, summary, detail));
    }

    private void generateMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, summary, detail));
    }

    private void generateMessageInDialog(FacesMessage.Severity severity, String summary, String detail) {
        FacesMessage message = new FacesMessage(severity, summary, detail);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }

    /**
     * To show an INFO notification message in main screen
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateMainInfoMessage(String summary, String detail) {
        this.generateMainMessage(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    /**
     * To show an ERROR notification message in main screen
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateMainErrorMessage(String summary, String detail) {
        this.generateMainMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    /**
     * To show an WARN notification message in main screen
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateMainWarnMessage(String summary, String detail) {
        this.generateMainMessage(FacesMessage.SEVERITY_WARN, summary, detail);
    }

    /**
     * To show an INFO notification message in current screen
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateInfoMessage(String summary, String detail) {
        this.generateMessage(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    /**
     * To show an ERROR notification message in current screen
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateErrorMessage(String summary, String detail) {
        this.generateMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    /**
     * To show an WARN notification message in current screen
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateWarnMessage(String summary, String detail) {
        this.generateMessage(FacesMessage.SEVERITY_WARN, summary, detail);
    }

    /**
     * To show an INFO notification message in a dialog window
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateInfoMessageInDialog(String summary, String detail) {
        this.generateMessageInDialog(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    /**
     * To show an ERROR notification message in a dialog window
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateErrorMessageInDialog(String summary, String detail) {
        this.generateMessageInDialog(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    /**
     * To show an WARN notification message in a dialog window
     *
     * @param summary Summary for the notification
     * @param detail Detail for the notification
     */
    protected void generateWarnMessageInDialog(String summary, String detail) {
        this.generateMessageInDialog(FacesMessage.SEVERITY_WARN, summary, detail);
    }

}
