package cl.gechs.cursojava.beans.notifications;

import cl.gechs.cursojava.model.EmailNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Carlos Soza <carlos.soza at gmail.com>
 */
public class EmailNotificationModel extends LazyDataModel<EmailNotification> {

    private static final long serialVersionUID = 6202606942101363422L;
    private final transient EmailNotificationController controller;

    public EmailNotificationModel(EmailNotificationController controller) {
        this.controller = controller;
    }

    @Override
    public EmailNotification getRowData(String rowKey) {
        return this.controller.getUtilsBean().getEmailNotificationService().getEmailNotification(rowKey);
    }

    @Override
    public Object getRowKey(EmailNotification emailNotification) {
        return emailNotification.getEmailNotificationId();
    }

    @Override
    public List<EmailNotification> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        List<EmailNotification> data = new ArrayList<>();
        List<EmailNotification> emailNotifications = this.controller.getUtilsBean().getEmailNotificationService().getEmailNotifications();
        if (emailNotifications != null) {
            data.addAll(emailNotifications);
        }
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
        return data;
    }
}
