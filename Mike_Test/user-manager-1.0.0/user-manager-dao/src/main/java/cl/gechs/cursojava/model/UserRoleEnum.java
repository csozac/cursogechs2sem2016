package cl.gechs.cursojava.model;

import java.io.Serializable;

/**
 *
 * @author Carlos Soza Canales <carlos.soza at gmail.com>
 */
public enum UserRoleEnum implements Serializable {

    ADMINISTRADOR("ADMINISTRADOR", "ADMINISTRADOR", "Administrador"),
    PARTICIPANTE("PARTICIPANTE", "PARTICIPANTE", "Participante");
    private final String id;
    private final String nemo;
    private final String nombre;

    UserRoleEnum(String id, String nemo, String nombre) {
        this.id = id;
        this.nemo = nemo;
        this.nombre = nombre;
    }

    public String getId() {
        return this.id;
    }

    public String getNemo() {
        return this.nemo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public static UserRoleEnum getById(String idRol) {
        for (UserRoleEnum tipo : UserRoleEnum.values()) {
            if (tipo.getId().equalsIgnoreCase(idRol)) {
                return tipo;
            }
        }
        return null;
    }

    public static UserRoleEnum getByNombre(String nombre) {
        for (UserRoleEnum tipo : UserRoleEnum.values()) {
            if (tipo.getNombre().equalsIgnoreCase(nombre)) {
                return tipo;
            }
        }
        return null;
    }
}
